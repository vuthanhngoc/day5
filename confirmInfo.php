<?php

if ($_SERVER['REQUEST_METHOD'] === "GET") {
    echo "Oops, sai đường rồi, <a href='register.php'>Quay lại</a>";
    exit();
}
$sex = array(0 => "Nam", 1 => "Nữ");
$spec = array("MAT" => "Khoa học máy tính", "KDL" => "Khoa học vật liệu");
define("UPLOAD_FOLDER", 'uploads/');
$data = array();

$fullNameVal = $addressVal = 'll';
$specVal = $sexVal = $dateVal = NULL;
$fileDestination = '';
if (isset($_POST['name'])) {
    $fullNameVal = $_POST['name'];
}
if (isset($_POST['sex'])) {
    $sexVal = $_POST['sex'];
}
if (isset($_POST['spec'])) {
    $specVal = $_POST['spec'];
}
if (isset($_POST['date'])) {
    $dateVal = $_POST['date'];
    $dateVal = date("d/m/Y", strtotime($dateVal));
}
if (isset($_POST['address'])) {
    $addressVal = $_POST['address'];
}
if (isset($_FILES['file'])) {
    $fileName = $_FILES['file']['name'];
    $fileTmpName = $_FILES['file']['tmp_name'];
    $filType = $_FILES['file']['type'];
    $fileError = $_FILES['file']['error'];
    $fileSize = $_FILES['file']['size'];

    $fileExt = explode('.', $fileName);
    $fileActualExt = strtolower(end($fileExt));
    if ($fileError === 0) {
        $fileNameNew = uniqid('', true);
        $fileDestination = UPLOAD_FOLDER . $fileNameNew . "." . $fileActualExt;
        if (!file_exists(UPLOAD_FOLDER)) {
            mkdir(UPLOAD_FOLDER, 0777, true);
        }
        if (!move_uploaded_file($fileTmpName, $fileDestination)) {
            // Moves an uploaded file to a new location error
        }
    } else {
        // image upload error
    }
}
$data = array(
    'fullName' => $fullNameVal,
    'sex' => $sex[$sexVal], 'spec' => $spec[$specVal],
    'birthDay' => $dateVal, 'address' => $addressVal,
    'imageUrl' => $fileDestination
);
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
    p {
        margin: 0;
    }

    .wrapper {
        display: flex;
        flex-direction: column;
        width: 50%;
        margin: auto;
        margin-top: 100px;
        padding: 50px 100px;
        /* background-color: #999;
         */
        border: solid 2px #007bc7;
    }

    .form-item {
        font-size: 18px;
        width: 100%;
        display: flex;
        margin-bottom: 12px;
    }

    .form-item p {
        width: 15%;
        height: 10%;
        padding: 12px 10px 5px 10px;
        border: solid 2px #007bc7;
        background-color: #4ba3ff;
        margin-right: 15px;
        color: white;
        text-align: center;
    }

    input[type="text"] {
        width: 50%;
        border: solid 2px #007bc7;
        outline: none;
    }

    input[type="date"] {
        border: solid 2px #007bc7;
        outline: none;
    }

    input[type="radio"] {
        margin-right: 12px;
    }

    .sex {
        display: flex;
        align-items: center;
        margin-left: 10px;
    }

    select {
        border: solid 2px #007bc7;
        outline: none;
    }

    .submit-wrapper {
        justify-content: center;
        margin-top: 45px;
    }

    input[type="submit"] {
        padding: 12px 30px;
        background-color: #39bc64;
        border-radius: 10px;
        border: solid 2px #007bc7;
        cursor: pointer;
        color: white;
        font-size: 17px;
    }

    input[type="file"] {
        margin-top: 10px;
    }

    i {
        color: red;
    }

    .error {
        margin-bottom: 8px;
        font-size: 18px;
        color: red;
    }

    font {
        display: flex;
        align-items: center;
    }
    </style>
</head>

<body>
    <div class="wrapper">
        <!-- <form action="" method="GET"> -->
        <div class="form-item">
            <p>Họ và tên</p>
            <font>
                <?= htmlspecialchars($data['fullName']) ?>
            </font>
        </div>
        <div class="form-item">
            <p>Giới tính</p>
            <div class="sex">
                <font>
                    <?= htmlspecialchars($data['sex']) ?>
                </font>
            </div>
        </div>
        <div class="form-item">
            <p>
                <label for="spec">
                    Phân khoa
                </label>
            </p>
            <font>
                <?= htmlspecialchars($data['spec']) ?>
            </font>
        </div>
        <div class="form-item">
            <p>
                <label for="date">
                    Ngày sinh
                </label>
            </p>
            <font>
                <?= htmlspecialchars($data['birthDay']) ?>
            </font>
        </div>
        <div class="form-item">
            <p>
                <label for="address">
                    Địa chỉ
                </label>
            </p>
            <font>
                <?= htmlspecialchars($data['address']) ? htmlspecialchars($data['address']) : "Không có địa chỉ" ?>
            </font>
        </div>
        <div class="form-item">
            <p>
                <label for="file">
                    Hình ảnh
                </label>
            </p>
            <?php echo !$data['imageUrl'] ? "<font>Không có ảnh</font>" : "<img src=${data['imageUrl']} alt='image'
                style='object-fit:cover;
                width:160px;
                height:80px;
                border: solid 1px #CCC' >" ?>

        </div>
        <div class="form-item submit-wrapper">
            <input type="submit" value="Xác nhận">
        </div>
        <!-- </form> -->
    </div>
</body>

</html>